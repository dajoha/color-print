.PHONY: before-publish
before-publish: test readme doc
	@ git status

.PHONY: test
test:
	@ cargo test --features terminfo
	@ cargo test

.PHONY: doc
doc:
	@ cargo doc --features terminfo
	@ cargo doc

.PHONY: docpriv
docpriv:
	@ cargo doc --features terminfo --document-private-items
	@ cargo doc --document-private-items

# Needs https://docs.rs/cargo-readme/latest/cargo_readme
# Install: `cargo install cargo-readme`
.PHONY: readme
readme:
	@ cd color-print; cargo readme > ../README.md
	@ sed -i 's/\[\(`[^`]*`\)]/\1/g' README.md
	@ cp README.md color-print
